=====================================
Automation of Gravelamps using Asimov
=====================================

It is often the case that a user of Gravelamps may wish to set up a large number of parameter estimation runs using the ``gravelamps_inference`` program. In such a case, writing many INI files would be tedious and would leave a significant potential for accidental error or incompatibility between one INI and another not allowing for easy comparison. To alleviate the burden of large volume analyses, Gravelamps has been made compatible with the `Asimov <https://asimov.docs.ligo.org/asimov>`_ automation framework.

Basic Example
=============

The full technical implementation of Gravelamps may be seen in the ``asimov.py`` located in the main code folder, but knowledge of this file is not necessary for using Gravelamps through Asimov. We refer the reader to the `Asimov documentation <https://asimov.docs.ligo.org/asimov>`_ for general information on using it. A typical ``analysis.yaml`` file to encode a Gravelamps run may look something like

.. code-block:: yaml

  kind: analysis
  name: GravelampsExample
  pipeline: gravelamps
  template: example-template

More Advanced Example 
=====================

When doing multiple model comparison, multiple Gravelamps runs may be necesary and linked together. In such cases an example ``analysis.yaml`` file may look something like:

.. code-block:: yaml

  kind: analysis
  name: GravelampsPoint
  pipeline: gravelamps
  template: example-template
  lens model: point
  ---
  kind: analysis
  name: GravelampsSIS
  pipeline: gravelamps
  template: example-template
  lens model: sis
  needs:
    - GravelampsPoint
  interest threshold: 1.0
  ---
  kind: analysis
  name: GravelampsNFW
  pipeline: gravelamps
  template: example-template
  lens model: nfw
  needs:
    - GravelampsPoint
  interest threshold: 1.0

This would generate three runs, one with the point, SIS, and NFW lens models. Firstly the point mass run would be done to establish a baseline. Then the other two model runs would be done. These could then be compared to the point mass run and for instance, the user could be alerted if the log Bayes Factor from the run was higher than 1.0.
