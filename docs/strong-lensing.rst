==================================
Model Selection for Strong Lensing
==================================

.. warning::
   This program requires the installation of the `GOLUM` package which must be installed using pip and is not installed by default when installing Gravelamps.

In the case of Strong Lensing, there are multiple resolvable images that must be considered together in order to gain information about the lensing model and parameters. With these multiple images, a full model specific joint parameter estimation framework would be computationally intractable, so instead Gravelamps seems to perform its model selection based on the output of the strong lensing pipeline GOLUM> 

This program requires that informtion about the event(s) be constructed into a JSON file---an example of which is given below. The program will gather the results from this event and calculate the evidence for each of the implemented models.

The program may be run as follows::

        $ gravelamps_strong_lensing -o --option /path/to/event.json

with a list of the possible options being given below.

Program Options
===============

-h, --help              Shows the help message and exits
-p, --plot              Generates and saves plot data 
--n-points              Number of soure position points to generate for the interpolator
--n-bins                Number of bins to use in histogram generation for parameter probability interpolator
--sample-dist           Calculates sample evidence contribution for every n samples

Example JSON
------------

.. code-block:: json

        {"GW000000-GW000001": {
          "channel_dict": {
            "H1": "H1:CHANNEL",
            "L1": "L1:CHANNEL",
            "V1": "V1:CHANNEL"
            },
          "duration": 4,
          "trigger_time_1": 0.0,
          "trigger_time_2": 0.1,
          "sampling_frequency": 2048,
          "waveform_arguments": {
            "reference_frequency": 20,
            "waveform_approximant": "IMRPhenomXPHM",
            "minimum_frequency": 20,
            "maximum_frequency": 896
            }
          "psd_length": 32
          "golum_result_file": "/path/to/golum/result/file.json" 
          }
        }
