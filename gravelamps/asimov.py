"""Asimov Pipeline Integration

Following is the implementation of Gravelamps as an Asimov pipeline based on the generic
instructions provided by Daniel Williams within the Asimov documentation. This sets up the
configuration required to automate Gravelamps running within the Asimov framework, allowing event
handling to be automated.

Written by Daniel Williams,
           Mick Wright.
"""

import configparser
import glob
import os
import re
import smtplib
import subprocess
import time

from asimov import config
from asimov.pipeline import Pipeline, PipelineException, PipelineLogger
from bilby.core.result import read_in_result

class Gravelamps(Pipeline):
    """
    Gravelamps specific Pipeline configuration.

    Based primarily upon Asimov's built-in Bilby Pipeline class. This handles building and
    submtiting individual event runs from a properly configured ledger.

    Methods
    -------
    detect_completion
        Assess if job has completed
    before_submit
        Pre submission hook
    build_dag
        Build Gravelamps DAG
    submit_dag
        Submits Gravelamps DAG to HTCondor
    collect_assets
        Collect result assets
    samples
        Collect result sample files for PESummary
    after_completion
        Post completion hook to run PESummary
    collect_logs
        Collect logs into dictionary
    check_progress
        Checks job progress

    """

    name = "Gravelamps"
    status = {"wait", "stuck", "stopped", "running", "finished"}

    def __init__(self, production, category=None):
        super(Gravelamps, self).__init__(production, category)
        self.logger.info("Using the Gravelamps pipeline")

        if not production.pipeline.lower() == "gravelamps":
            raise PipelineException

    def detect_completion_processing(self):
        return True

    def after_processing(self):
        self.production.status = "uploaded"

    def detect_completion(self):
        """
        Assess if job has completed.

        The Gravelamps DAG's final job is always the bilby_pipe DAG. To assess if the DAG has
        completed therefore, the function checks for the existance of the final result file in the
        ouput directory to assert the completion of the job.

        Returns
        -------
        bool
            Job completion status---true if complete, false otherwise.
        """

        self.logger.info("Checking if the bilby parameter estimation has completed")
        results_dir = glob.glob(f"{self.production.rundir}/result")

        if len(results_dir) > 0:
            result_files = glob.glob(
                os.path.join(results_dir[0], "*merge*_result.hdf5"))
            result_files += glob.glob(
                os.path.join(results_dir[0], "*merge*_result.json"))
            self.logger.info(f"Result Files: {result_files}")

            if len(result_files) > 0:
                self.logger.info("Result file(s) found, job completed")
                return True
            else:
                self.logger.info("Result file not found")
                return False

        self.logger.info("Result directory not found")
        return False

    def before_submit(self):
        """
        Pre submission hook.

        The hook at present adds the preserve relative file path argument to the condor submission
        file.

        Notes
        -----
        The hook currently adds the results directory from bilby_pipe to the individual submission
        files that transfer input files. This is to deal with a current ongoing issue in bilby_pipe
        that is due to be fixed in the next release, and will be modified after this occurs.
        """

        self.logger.info("Running pre-submit hook")

        submission_files = glob.glob(f"{self.production.rundir}/submit/*.submit")

        for sub_file in submission_files:
            if "dag" in sub_file:
                continue
            with open(sub_file, "r", encoding="utf-8") as file:
                original = file.readlines()
            with open(sub_file, "w", encoding="utf-8") as file:
                self.logger.info(f"Adding preserve relative_paths to {sub_file}")
                file.write("preserve_relative_paths = True\n" + ''.join(original))


    def build_dag(self, psds=None, user=None, clobber_psd=None, dryrun=False):
        """
        Build Gravelamps DAG.

        Construct a DAG file in order to submit a production to the condor scheduler
        using gravelamps_inference.

        Parameters
        ----------
        production : str
            Production name
        psds : dict, optional
            The PSDs which should be used for this DAG. If no PSDs are provided
            the PSD files specified in the ini file will be used instead
        user : str
            The user accounting tag which should be used to run the job
        dryrun : bool
            If set to true the commands will not be run, but will be printed
            to standard output. Defaults to False

        Raises
        ------
        PipelineException
            Raised if the construction of the DAG fails
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")

        if self.production.event.repository:
            ini =\
                self.production.event.repository.find_prods(self.production.name, self.category)[0]
            ini = os.path.join(cwd, ini)
        else:
            ini = f"{self.production.name}.ini"

        if self.production.rundir:
            rundir = self.production.rundir
        else:
            rundir = os.path.join(os.path.expanduser("~"),
                                  self.production.event.name,
                                  self.production.name)
            self.production.rundir = rundir

        command = [
            os.path.join(config.get("pipelines", "environment"), "bin", "gravelamps_inference"),
            ini]

        if dryrun:
            print(" ".join(command))
        else:
            self.logger.info(" ".join(command))
            pipe = subprocess.Popen(command,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
            out, err = pipe.communicate()
            self.logger.info(out)

            if err or "To submit, use the following command:" not in str(out):
                self.production.status = "stuck"
                self.logger.error(err)
                raise PipelineException(
                    f"DAG file could not be created.\n{command}\n{out}\n{err}",
                    production=self.production.name)

            else:
                time.sleep(10)
                return PipelineLogger(message=out, production=self.production.name)

    def submit_dag(self, dryrun=False):
        """
        Submits DAG file to the condor cluster

        Parameters
        ----------
        dryrun : bool
            If set to true the DAG will not be submitted but all commands will be
            printed to standard output instead. Defaults to False

        Returns
        -------
        int
            The cluster ID assigned to the running DAG file
        PipelineLogger
            The pipeline logger message.

        Raises
        ------
        PipelineException
            This will be raised if the pipeline fails to submit the job
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")

        self.before_submit()

        try:
            dag_filename = "gravelamps_inference.dag"

            command = ["condor_submit_dag",
                       "-batch-name",
                       f"gravelamps/{self.production.event.name}/{self.production.name}",
                       os.path.join(self.production.rundir, "submit", dag_filename)]

            if dryrun:
                print(" ".join(command))
            else:
                dagman = subprocess.Popen(command,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.STDOUT)

                self.logger.info(" ".join(command))

                stdout, stderr = dagman.communicate()

                if "submitted to cluster" in str(stdout):
                    cluster = re.search(r"submitted to cluster ([\d]+)", str(stdout)).groups()[0]
                    self.logger.info(f"Submitted successfully. Running with job ID {int(cluster)}")

                    self.production.status = "running"
                    self.production.job_id = int(cluster)

                    return cluster, PipelineLogger(stdout)
                else:
                    self.logger.error("Could not submit the job to the cluster")
                    self.logger.info(stdout)
                    self.logger.error(stderr)

                    raise PipelineException("The DAG file could not be submitted")

        except FileNotFoundError as error:
            self.logger.exception(error)
            raise PipelineException("It appears HTCondor is not installed on this system.\n"
                                    f'''Command that would have been run: {" ".join(command)}.''')\
                                    from error

    def collect_assets(self):
        """
        Collect result assets.

        The current result assests are deemed to be the samples produced by the nested sampling
        run.
        """

        return {"samples": self.samples()}

    def samples(self, absolute=True):
        """
        Collect the combined samples file for PESummary

        Parameters
        ----------
        absolute : bool
            Flag to return the absolute or relative filepath

        Returns
        -------
        sample_files : str
            Path to the combined sample file
        """

        if absolute:
            rundir = os.path.abspath(self.production.rundir)
        else:
            rundir = self.production.rundir
        self.logger.info(f"Rundir for samples: {rundir}")

        sample_files = glob.glob(os.path.join(rundir, "result", "*_merge*_result.hdf5"))\
                       + glob.glob(os.path.join(rundir, "result", "*_merge*_result.json"))

        return sample_files

    def after_completion(self):
        """
        Tasks to run after the job is complete.

        These tasks are to read the Gravelamps dependency to find whether or not the result is
        interesting and to set the flag accordingly. If the job is interesting the job will also
        email the user if that flag is set
        """

        super().after_completion()
        self.logger.info("Job has completed")
        self.production.status = "processing"

        if "comparison method" in self.production.meta:
            run_result_file = self.samples()[0]
            run_result = read_in_result(filename=run_result_file)
            run_evidence = run_result.log_10_evidence
            run_noise = run_result.log_10_noise_evidence
            run_ini = self.read_ini(self.get_ini())
            interest_threshold = run_ini.getfloat("asimov_settings", "interest_threshold")
            
            self.logger.info(f"Checking result against interest threshold of {interest_threshold}")

            if self.production.meta["comparison method"] == "dependency":
                self.logger.info("Comparing against dependency production")
                comparison_production = self.get_comparison_production()

                if comparison_production is not None:
                    comparison_model, comparison_evidence, comparison_noise =\
                        self.get_comparison_data(comparison_production)
                else:
                    comparison_model = None
                    comparison_noise = None
                    comparison_evidence = None

            elif self.production.meta["comparison method"] == "unlensed ledger":
                self.logger.info("Comparing against metadata production")
                if "unlensed pe info" not in self.production.meta:
                    raise KeyError("Could not locate unlensed pe info in metadata")

                result_file = self.production.meta["unlensed pe info"]["result file path"]
                result_obj = read_in_result(result_file)

                comparison_evidence = result_obj.log_10_evidence
                comparison_noise = result_obj.log_10_noise_evidence
                comparison_model = "unlensed"

            if comparison_noise is not None:
                if run_noise == comparison_noise:
                    log_bayes = run_evidence - comparison_evidence
                    self.logger.info(f"Bayes Factor: {log_bayes}")
                    if log_bayes > interest_threshold:
                        self.logger.info("Event deemed interesting")
                        interest_status = True
                    else:
                        self.logger.info("Event deemed uninteresting")
                        interest_status = False
                else:
                    self.logger.info("Run noise does not match")
                    interest_status = False
            else:
                self.logger.info("Comparison production not found")
                interest_status = False

            self.production.meta["interest status"] = interest_status
            email = run_ini.getboolean("asimov_settings", "email", fallback=False)

            if interest_status and email:
                self.logger.info("Sending alert email")
                self.interest_email(comparison_model, run_ini, log_bayes)
	
        self.production.event.update_data()

    def interest_email(self, model, ini, log_bayes):
        """
        Sends an email to interested parties when job is determined to have interesting results
        """

        self.logger.info("Sending email to inform interest")

        sender = f"{config.get('condor', 'user')}@ligo.org"
        receivers = list(ini.get('asimov_settings', 'alert-receivers').strip().split(","))

        run_model = self.production.meta["lens model"]

        subject = f"{self.production.event.name}"
        message = f"Gravelamps production {self.production.name} has completed on"\
                  f" {self.production.event.name} and has been deemed interesting."\
                  f" The log Bayes factor comparing {run_model} and {model} is"\
                  f" {log_bayes}."

        full_email = f"Subject: {subject} \n\n {message}"

        email_object = smtplib.SMTP("localhost")
        email_object.sendmail(sender, receivers, full_email)

    def get_ini(self):
        """
        Retrieve INI for the production
        """

        cwd = os.getcwd()
        if self.production.event.repository:
            ini =\
                self.production.event.repository.find_prods(self.production.name, self.category)[0]
            ini = os.path.join(cwd, ini)
        else:
            ini = f"{self.production.name}.ini"

        return ini


    def get_comparison_production(self):
        """
        Get the comparison production from the dependecies
        """

        if not self.production.dependencies:
            return None

        dependency_list = []
        for dependency in self.production.dependencies:
            dependency_list.append(dependency.name)

        for production in self.production.event.productions:
            if production.name in dependency_list and production.status in ("uploaded", "finished"):
                self.logger.info(f"Matched with Production: {production.name}")
                return production

        self.logger.info(f"Did not match any productions")
        return None

    def get_comparison_data(self, comparison_production):
        """
        Get the information from the comparison production
        """
        
        rundir = comparison_production.rundir
        result_files = glob.glob(os.path.join(rundir, "result", "*_merge*_result.hdf5"))\
                       + glob.glob(os.path.join(rundir, "result", "*_merge*_result.json"))
        result = read_in_result(result_files[0])


        if str(comparison_production.pipeline).lower() == "bilby":
            comparison_model = "unlensed"
        elif str(comparison_production.pipeline).lower() == "gravelamps":
            comparison_model = comparison_production.meta["lens model"]
        else:
            comparison_model = None

        comparison_evidence = result.log_evidence
        comparison_noise = result.log_noise_evidence

        return comparison_model, comparison_evidence, comparison_noise

    def collect_logs(self):
        """
        Collect all of the log files which have been produced by this production and return
        their contents as a dictionary

        Returns
        -------
        messages : dict
            Dictionary containing the log file content or notification that the file could not
            be opened
        """

        logs = glob.glob(f"{self.production.rundir}/submit/*.err")\
               + glob.glob(f"{self.production.rundir}/log*/*.err")\
               + glob.glob(f"{self.production.rundir}/*/*.out")\
               + glob.glob(f"{self.production.rundir}/*.log")

        messages = {}

        for log in logs:
            try:
                with open(log, "r", encoding="utf-8") as log_f:
                    message = log_f.read()
                    message = message.split("\n")
                    messages[log.split("/")[-1]] = "\n".join(message[-100:])
            except FileNotFoundError:
                messages[log.split("/")[-1]] = "There was a problem opening this log file"

        return messages

    def check_progress(self):
        """
        Checks job progress.

        The job progress is checked up on by finding the number of iterations and the current value
        of the dlogz for the sampling runs. This combined information can be used to obtain a rough
        estimate of how far through the job the run is. This is returned in dictionary format.

        Returns
        -------
        messages : dict
            Dictionary containing job progress in the form of the number of iterations and current
            dlogz value. Will contain a message noting if the log file for the job could not be
            opened.
        """

        logs = glob.glob(f"{self.production.rundir}/log_data_analysis/*.out")

        messages = {}
        for log in logs:
            try:
                with open(log, "r", encoding="utf-8") as log_f:
                    message = log_f.read()
                    message = message.split("\n")[-1]
                    pat = re.compile(r"([\d]+)it")
                    iterations = pat.search(message)
                    pat = re.compile(r"dlogz:([\d]*\.[d]*)")
                    dlogz = pat.search(message)
                    if iterations:
                        messages[log.split("/")[-1]] = (iterations.group(),
                                                        dlogz.group())
            except FileNotFoundError:
                messages[log.split("/")[-1]] = "There was a problem opening this log file."

        return messages

    @classmethod
    def read_ini(cls, filepath):
        """
        Read and parse Gravelamps configuration file.

        Gravelamps configuration files are INI compliant, with dedicated and important sections
        Individual options can be repeated between sections.

        Returns
        -------
        config_parser : ConfigParser
            Object containing Gravelamps configuration settings based on the INI structure.
        """

        config_parser = configparser.ConfigParser()
        config_parser.read(filepath)

        return config_parser

    def html(self):
        """
        Return the HTML representation of this pipeline
        """

        pages_dir = os.path.join(self.production.event.name, self.production.name, "pesummary")
        out = ""

        if self.production.status in {"uploaded"}:
            out += """<div class="asimov-pipeline">"""
            out += f"""<p><a href="{pages_dir}/home.html">Summary Pages</a></p>"""
            out +=\
                f"""<img height=200 src="{pages_dir}/plots/{self.production.name}"""\
                """_psd_plot.png"</src>"""
            out +=\
                f"""<img_height=200 stc="{pages_dir}/plots/{self.production.name}"""\
                """_waveform_time_domain.png"</src>"""
            out += """</div>"""

        return out

    def resurrect(self):
        """
        Attempt to ressurect a failed job.

        A failed job will be resurrected a maximum of five times assuming that a rescue DAG
        has been produced. 
        """

        try:
            count = self.production.meta["resurrections"]
        except KeyError:
            count = 0

        if count < 5 and\
           len(glob.glob(os.path.join(self.production.rundir, "submit", "*.rescue"))) > 0:
            count += 1
            self.submit_dag()
