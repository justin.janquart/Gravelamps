"""Singular Isothermal Sphere (SIS) Lensing Functions

Following are functions performing calculations for the singular isothermal sphere lens mass
density profile model. The module backend is based in libsis.

Written by Mick Wright 2022

Globals
-------
_cdll : ctypes.CDLL
    Library of C++ functions necessary for calculations
_additional_arguments : list of str
    Additional arguments required to construct interpolator data
_additional_argument_types : list of types
    Types of the argumnets that are given above
_lens_parameters : list of str
    Parameters used for the model

Routines
--------

redshifted_lens_mass
    Calculates redshifted lens mass required for given time delay and dimensionless time delay to
    correlate
dimensionless_time_delay
    Computes the dimensionless time delay corresponding to a given source position
magnification
    Computes the magnification of the specified image at the given source position
single_image_amplification
    Computes the geometric optics amplification factor for only the specified image
amplification_factor
    Calculates geometric optics amplification factor
generate_interpolator_data
    Generates the amplification factor data files for use in interpolator generation
"""

import ctypes

from importlib.resources import files

import numpy as np

from gravelamps.core.conversion import natural_mass_to_solar_mass
from gravelamps.core.gravelog import gravelogger
from .generic import get_additional_arguments

#The following loads in the DLL containing the C++ functions for the direct implementations for
#geometric optics runs. It then sets the argument and result types accordingly

_cdll = ctypes.CDLL(files('gravelamps').joinpath('model/lib/libsis.so'))

_cdll.PyMagnification.argtypes = (ctypes.c_double, ctypes.c_int)
_cdll.PyMagnification.restype = ctypes.c_double

_cdll.PyTimeDelay.argtypes = (ctypes.c_double,)
_cdll.PyTimeDelay.restype = ctypes.c_double

_cdll.PyAmplificationFactorGeometric.argtypes = (ctypes.c_double, ctypes.c_double)
_cdll.PyAmplificationFactorGeometric.restype = ctypes.POINTER(ctypes.c_double)

_cdll.PySingleImageAmplification.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_int)
_cdll.PySingleImageAmplification.restype = ctypes.POINTER(ctypes.c_double)

_cdll.GenerateLensData.argtypes = (ctypes.c_char_p,
                                   ctypes.c_char_p,
                                   ctypes.c_char_p,
                                   ctypes.c_char_p,
                                   ctypes.c_int,
                                   ctypes.c_int,
                                   ctypes.c_int)
_cdll.GenerateLensData.restype = ctypes.c_int

#Additional arguments necessary for the running of the executable in addition to the files for the
#interpolator construction
_additional_arguments = ["sis_summation_upper_limit",
                         "arithmetic_precision",
                         "geometric_optics_frequency"]
_additional_argument_types = [int, int, int]

#Parameters for the model
_lens_parameters = ["lens_mass", "lens_fractional_distance", "source_position"]

SOURCE_MIN = 0.01
SOURCE_MAX = 0.99

def redshifted_lens_mass(time_delay_value, dimensionless_time_delay_value):
    """
    Calculates redshifted lens mass required for given time delay and dimensionless time delay to
    correlate.

    This is a rearranging of the relationship betweeen the time delay and the dimensionless
    equivalent, which for the isolated singular isothermal sphere model is given by
    td = 4 * Mlz * dim_td.

    Parameters
    ----------
    time_delay_value : float
        Physical time delay between two signals in seconds
    dimensionless_time_delay_value : float
        Corresponding dimensionless time delay between the two signals

    Returns
    -------
    mass : float
        Physical mass that connects the physical and dimensionless time delays
    """

    dimensionless_mass = time_delay_value /(4 * dimensionless_time_delay_value)
    mass = natural_mass_to_solar_mass(dimensionless_mass)

    return mass

def magnification(source_position, image):
    """
    Computes the magnification of the specified image at the source position given.

    This is a wrapper function to the C++ function `PyMagnification` within `libsis`.

    Parameters
    ----------
    source_position : float
        Dimensionless displacement from the optical axis
    image : int
        Which image to compute. 1 corresponds to the + image and 0 corresponds to the - image.

    Returns
    -------
    float
        Magnification corresponding to the specified input parameters
    """

    if image not in (0, 1):
        raise ValueError("Image must be either 1 or 0 to indicate the + or - image!")

    c_mag = _cdll.PyMagnification(ctypes.c_double(source_position),
                                  ctypes.c_int(image))

    mag = float(c_mag)
    return mag

def relative_magnification(source_position):
    """
    Computes the relative magnification of two images at the given source position.

    This function computes the two magnifications associated with singular isothermal sphere
    lensing and returns the ratio (+/-) between them.

    Parameters
    ----------
    source_position : float
        Dimensionless displacement from the optical axis

    Returns
    -------
    mu_rel : float
        Ratio between the magnifications of the + and - image respectively. 
    """

    mu_plus = magnification(source_position, 1)
    mu_minus = magnification(source_position, 0)

    mu_rel = mu_plus/mu_minus

    return mu_rel

def dimensionless_time_delay(source_position):
    """
    Computes the dimensionless time delay that corresponds to the source position.

    This is a wrapper function to the C++ function `PyTimeDelay` within `libsis` for the given
    source position. 

    Parameters
    ----------
    source_position : float
        Dimensionless displacement from the optical axis

    Returns
    -------
    delay : float
        Dimensionless time delay produced by the lensing of the signals with given source position
    """

    delay = float(_cdll.PyTimeDelay(ctypes.c_double(source_position)))

    return delay

def single_image_amplification(dimensionless_frequency_array, source_position, image):
    """
    Calculates the geomtric optics amplification factor for only the specified image.

    This calculation is done using the C++ function `PySingleImageAmplification` within `libsis`
    for the given dimensionless frequencies and source position, for the specified image.

    Parameters
    ----------
    dimensionless_frequency_array : ArrayLike
        Dimensionless form of the frequencies of interest
    source_position : float
        Dimensionelss displacement from the optical axis
    image : int
        Which image to compute, 1 corresponds to the + image and 0 corresponds to the - image

    Returns
    -------
    amplification_array : ArrayLike
        Amplification of the image signal for the frequeny range at the specified source position
    """

    amplification_array = np.empty(len(dimensionless_frequency_array), dtype=complex)

    for idx, dimensionless_frequency in enumerate(dimensionless_frequency_array):
        c_result = _cdll.PySingleImageAmplification(ctypes.c_double(dimensionless_frequency),
                                                    ctypes.c_double(source_position),
                                                    ctypes.c_int(image))
        amplification_array[idx] = complex(c_result[0], c_result[1])
        _cdll.destroyObj(c_result)

    return amplification_array

def amplification_factor(dimensionless_frequency_array, source_position):
    """
    Calculates geometric optics amplification factor.

    This calculation is done using C++ function `PyAmplificationFactorGeometric` within `libsis`
    for the given dimensionless frequency and source position.

    Parameters
    ----------
    dimensionless_frequency_array : Array of floats
        Dimensionless form of the frequencies of interest
    source_position : float
        Dimensionless displacement from the optical axis

    Returns
    -------
    amplification_array : Array of complex
        Amplification factor to the signal

    """

    amplification_array = np.empty(len(dimensionless_frequency_array), dtype=complex)

    for idx, dimensionless_frequency in enumerate(dimensionless_frequency_array):
        c_result = _cdll.PyAmplificationFactorGeometric(ctypes.c_double(dimensionless_frequency),
                                                        ctypes.c_double(source_position))
        amplification_array[idx] = complex(c_result[0], c_result[1])
        _cdll.destroyObj(c_result)

    return amplification_array

def generate_interpolator_data(config,
                               args,
                               file_dict):
    """
    Generates the amplification factor data files for use in interpolator generation.

    This is done via the C++ `GenerateLensData` function within `libsis`. It will read in the
    specified grid files and fill the data files with the appropriate values of the amplification
    factor. This can be done in wave and geometric optics.

    Parameters
    ----------
    config : configparser.ConfigParser
        Object containing settings from user INI file
    args : argparse.Namespace
        Object containing commandline arguments to program
    file_dict : dict
        Contains paths to the interpolator grid and data files to fill

    """

    additional_arguments = get_additional_arguments(config, args,
                                                    _additional_arguments,
                                                    _additional_argument_types)

    gravelogger.info("Generating Lens Interpolator Data")
    res = _cdll.GenerateLensData(
        ctypes.c_char_p(file_dict["dimensionless_frequency"].encode("utf-8")),
        ctypes.c_char_p(file_dict["source_position"].encode("utf-8")),
        ctypes.c_char_p(file_dict["amplification_factor_real"].encode("utf-8")),
        ctypes.c_char_p(file_dict["amplification_factor_imag"].encode("utf-8")),
        ctypes.c_int(additional_arguments[0]),
        ctypes.c_int(additional_arguments[1]),
        ctypes.c_int(additional_arguments[2]))

    res = int(res)

    if res == 85:
        gravelogger.info("Checkpoint Instruction Receieved")
    else:
        gravelogger.info("Lens Interpolator Data Generated")
    return res
