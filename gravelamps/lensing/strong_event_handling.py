"""
Gravelamps Strong Lensing Event Handling

Following are functions that are model agnostic and used to handle interactions between strong
lensing events and the model specific functions within the rest of the module.

Routines
--------
get_event_signal
    Generate interferometer signal data from provided information
get_model_generator
    Create Waveform Generator for specific model to be used for likelihood calculations
generate_source_position_interpolator 
    Generate interpolator function which maps relative magnification to source position.
observables_to_model_parameters
    Convert lensing observables to model specific parameters
generate_probability_interpolator 
    Generate interpolator function object mapping source position and redshifted lnes mass to the
    probability of both occurring.
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d, interp2d

from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters
from bilby.gw.detector import get_empty_interferometer, InterferometerList, PowerSpectralDensity
from bilby.gw.detector.strain_data import InterferometerStrainData
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.waveform_generator import WaveformGenerator
from golum.tools.utils import make_bbh_parameters_from_first_image_parameters_and_lensing_parameters
from golum.tools.waveformmodels import lensed_bbh_model

from gravelamps.core.gravelog import gravelogger
from gravelamps.lensing.waveform_generator import SingleImageGenerator

def get_event_signal(args, event_data):
    """
    Generate interferometer signal data from provided information

    User flags also allow the plotting of interferometric data and in the case of injections, the
    plotting of the true signal data.

    Parameters
    ----------
    args : argparse.Namespace
        Object containing commandline arguments to the program
    event_data : dict
        Information about the event

    Returns
    -------
    interferometers : bilby.gw.detector.InterferometerList
        Object containing interferometer data for specified detectors
    """

    if args.injection:
        true_source_parameters = event_data["source_parameters"]
        true_lensing_observables = event_data["lensing_observables"]

        injection_parameters =\
            make_bbh_parameters_from_first_image_parameters_and_lensing_parameters(
                true_source_parameters, true_lensing_observables)

        np.random.seed(event_data["generation_seed"])

        waveform_arguments = event_data["waveform_arguments"]
        waveform_generator = WaveformGenerator(duration=event_data["duration"],
                                               sampling_frequency=event_data["sampling_frequency"],
                                               frequency_domain_source_model=lensed_bbh_model,
                                               waveform_arguments=waveform_arguments)

        interferometers_1 = InterferometerList(event_data["detectors"])
        interferometers_1.set_strain_data_from_power_spectral_densities(
            sampling_frequency=event_data["sampling_frequency"],
            duration=event_data["duration"],
            start_time=true_source_parameters["geocent_time"]-3.)
        interferometers_1.inject_signal(waveform_generator=waveform_generator,
                                        parameters=true_source_parameters)

        interferometers_2 = InterferometerList(event_data["detectors"])
        interferometers_2.set_strain_data_from_power_spectral_densities(
            sampling_frequency=event_data["sampling_frequency"],
            duration=event_data["duration"],
            start_time=injection_parameters["geocent_time"]-3.)
        interferometers_2.inject_signal(waveform_generator=waveform_generator,
                                        parameters=injection_parameters)

    else:
        channel_dict = event_data["channel_dict"]

        duration = event_data["duration"]
        trigger_time_1 = event_data["trigger_time_1"]
        trigger_time_2 = event_data["trigger_time_2"]

        gps_start_time_1 = trigger_time_1 + 2 - duration
        psd_start_time_1 = trigger_time_1 - duration

        gps_start_time_2 = trigger_time_2 + 2 - duration
        psd_start_time_2 = trigger_time_2 - duration

        interferometers_1 = InterferometerList([])
        interferometers_2 = InterferometerList([])

        for detector in channel_dict:
            interferometer_1 = get_empty_interferometer(detector)
            interferometer_1.set_strain_data_from_channel_name(
                channel_dict[detector],
                sampling_frequency=event_data["sampling_frequency"],
                duration=duration,
                start_time=gps_start_time_1)
            interferometer_1.minimum_frequency=event_data["waveform_arguments"]["minimum_frequency"]
            interferometer_1.maximum_frequency=event_data["waveform_arguments"]["maximum_frequency"]

            strain_1 = InterferometerStrainData()
            strain_1.set_from_channel_name(channel_dict[detector],
                                          duration=event_data["psd_length"],
                                          start_time=psd_start_time_1,
                                          sampling_frequency=event_data["sampling_frequency"])
            freq_array, psd_array = strain_1.create_power_spectral_density(fft_length=4)

            interferometer_1.power_sepctral_density =\
                PowerSpectralDensity(frequency_array=freq_array, psd_array=psd_array)
            interferometers_1.append(interferometer_1)

            interferometer_2 = get_empty_interferometer(detector)
            interferometer_2.set_strain_data_from_channel_name(
                channel_dict[detector],
                sampling_frequency=event_data["sampling_frequency"],
                duration=duration,
                start_time=gps_start_time_2)
            interferometer_2.minimum_frequency=event_data["waveform_arguments"]["minimum_frequency"]
            interferometer_2.maximum_frequency=event_data["waveform_arguments"]["maximum_frequency"]

            strain_2 = InterferometerStrainData()
            strain_2.set_from_channel_name(channel_dict[detector],
                                           duration=event_data["psd_length"],
                                           start_time=psd_start_time_2,
                                           sampling_frequency=event_data["sampling_frequency"])
            freq_array, psd_array = strain_2.create_power_spectral_density(fft_length=4)

            interferometer_2.power_spectral_density =\
                PowerSpectralDensity(frequency_array=freq_array, psd_array=psd_array)
            interferometers_2.append(interferometer_2)

    if args.plot:
        if args.injection:
            strain = waveform_generator.frequency_domain_strain(injection_parameters)

            plt.plot(waveform_generator.frequency_array, np.abs(strain["plus"]), label="+")
            plt.plot(waveform_generator.frequency_array, np.abs(strain["cross"]), label="x")
            plt.xlabel("Frequency (Hz)")
            plt.ylabel("|h|")
            plt.savefig("golum-injection-waveform.png")
            plt.close()

        for interferometer in interferometers_2:
            interferometer.plot_time_domain_data()

    return interferometers_1, interferometers_2

def get_model_generator(args, module, event_data):
    """
    Create Waveform Generator for specific model to be used for likelihood calculations

    Flags will also allow the plotting of the frequency domain strain for injections

    Parameters
    ----------
    args : argparse.Namespace
        Object containing commandline arguments to the program
    module : module
        Module containing lens model functions
    event_data : dict
        Information about the event

    Returns
    -------
    lensed_waveform_generator : gravelamps.lensing.waveform_generator.SingleImageGenerator
        Object containing signal data
    """

    waveform_arguments = event_data["waveform_arguments"]
    waveform_arguments["lens_module"] = module.__name__
    waveform_arguments["use_redshifted_lens_mass"] = True
    waveform_arguments["image"] = 0

    lensed_waveform_generator_first =\
        SingleImageGenerator(duration=event_data["duration"],
                             sampling_frequency=event_data["sampling_frequency"],
                             frequency_domain_source_model=lal_binary_black_hole,
                             parameter_conversion=convert_to_lal_binary_black_hole_parameters,
                             waveform_arguments=waveform_arguments)

    waveform_arguments_image_2 = waveform_arguments.copy()
    waveform_arguments_image_2["image"] = 1

    lensed_waveform_generator_second =\
        SingleImageGenerator(duration=event_data["duration"],
                             sampling_frequency=event_data["sampling_frequency"],
                             frequency_domain_source_model=lal_binary_black_hole,
                             parameter_conversion=convert_to_lal_binary_black_hole_parameters,
                             waveform_arguments=waveform_arguments_image_2)

    if args.plot and args.injection:
        source_parameters = event_data["source_parameters"]
        lens_parameters = event_data["lens_parameters"]
        injection_parameters = source_parameters | lens_parameters

        magnification = module.magnification(lens_parameters["source_position"], 1)
        actual_distance = source_parameters["luminosity_distance"] * np.sqrt(abs(magnification))
        injection_parameters["luminosity_distance"] = actual_distance

        strain = lensed_waveform_generator_first.frequency_domain_strain(injection_parameters)

        plt.plot(lensed_waveform_generator_first.frequency_array,
                 np.abs(strain["plus"]), label="+")
        plt.plot(lensed_waveform_generator_first.frequency_array,
                 np.abs(strain["cross"]), label="x")
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("|h|")
        plt.savefig(f"{module.__name__.split('.')[-1]}-injection-waveform.png")

    return lensed_waveform_generator_first, lensed_waveform_generator_second

def generate_source_position_interpolator(module, source_position_array, plot=False):
    """
    Generate interpolator function which maps relative magnification to source position.

    Parameters
    ----------
    module : module
        Module containing lens model functions
    source_position_array : ArrayLike
        Source position range over which to generate interpolator
    plot : bool, optional
        Generate plot of data that forms interpolator. Default is false

    Returns
    -------
    source_position_interpolator : scipy.interpolate.interp1d
        Interpolating function taking relative magnification and returning source position
    """

    relative_magnification_map = map(module.relative_magnification, source_position_array)
    relative_magnification_array = np.abs(np.fromiter(relative_magnification_map, dtype=float))

    interpolator = interp1d(relative_magnification_array,
                            source_position_array)

    @np.vectorize
    def source_position_interpolator(mu_rel):
        try:
            return interpolator(mu_rel)
        except ValueError:
            try:
                return interpolator(1/mu_rel)
            except ValueError:
                return module.SOURCE_MIN

    if plot:
        plt.figure()
        plt.plot(source_position_array, relative_magnification_array)
        plt.xlabel("Source Position (y)")
        plt.ylabel("Relative Magnification $\\mu_{rel}$")
        plt.savefig(f"{module.__name__.split('.')[-1]}-source-position-rel-mag.png")
        plt.close()

    return source_position_interpolator

def observables_to_model_parameters(module, args, relative_magnification_data, time_delay_data):
    """
    Convert lensing observables to model specific parameters

    Parameters
    ----------
    module : module
        Module containing lens model functions
    args : argparse.Namespace
        Object containing commandline arguments to the program
    relative_magnification_data : ArrayLike
        Sample relative magnification values
    time_delay_data : ArrayLike
        Sample time delay values

    Returns
    -------
    source_position_data : ArrayLike
        Calculated sample source position values
    mass_data : ArrayLike
        Calculated sample redshited lens mass values
    """

    gravelogger.info("Converting lensing observables to model parameters")

    gravelogger.info("Generating source position data")
    source_position_array = np.linspace(module.SOURCE_MIN, module.SOURCE_MAX, args.n_points)
    source_position_interpolator =\
        generate_source_position_interpolator(module, source_position_array, plot=args.plot)
    source_position_data = source_position_interpolator(relative_magnification_data)

    gravelogger.info("Generating redshifted lens mass data")
    dimensionless_time_delay_map = map(module.dimensionless_time_delay, source_position_data)
    dimensionless_time_delay_data = np.fromiter(dimensionless_time_delay_map, dtype=float)
    mass_data = module.redshifted_lens_mass(time_delay_data, dimensionless_time_delay_data)

    gravelogger.info("Model parameters generated")
    gravelogger.info("Average source position: %f", np.mean(source_position_data))
    gravelogger.info("Average redshifted lens mass: %f", np.mean(mass_data))

    return source_position_data, mass_data

def generate_probability_interpolator(source_position_data,
                                      mass_data,
                                      number_of_bins=30,
                                      plot=False,
                                      model=None):
    """
    Generate interpolator function object mapping source position and redshifted lnes mass to the
    probability of both occurring.

    This is done by taking a normalised histogram of the occurrances of each parameter in the data.
    User flag allows the plotting of the histogram data that forms the basis fo the interpolator.
    """

    gravelogger.info("Generating parameter probability interpolator")

    cutoff_mass = np.mean(mass_data) + 6*np.std(mass_data)
    to_delete = np.where(mass_data > cutoff_mass)
    updated_mass_data = np.delete(mass_data, to_delete)
    updated_position_data = np.delete(source_position_data, to_delete)

    histogram_data, _, _ =\
        np.histogram2d(updated_position_data, updated_mass_data, bins=number_of_bins, density=True)

    interpolator_mass_range = np.linspace(updated_mass_data.min(),
                                          updated_mass_data.max(),
                                          number_of_bins)
    interpolator_position_range = np.linspace(updated_position_data.min(),
                                              updated_position_data.max(),
                                              number_of_bins)

    probability_interpolator = interp2d(interpolator_position_range,
                                        interpolator_mass_range,
                                        histogram_data)

    if plot:
        if model is None:
            gravelogger.warning("Histogram data being plotted without being told model")

        plt.pcolormesh(interpolator_position_range,
                       interpolator_mass_range,
                       histogram_data)
        plt.colorbar()
        plt.xlim(interpolator_position_range[0], interpolator_position_range[-1])
        plt.ylim(interpolator_mass_range[0], interpolator_mass_range[-1])
        plt.xlabel("Source Position (y)")
        plt.ylabel("Redshifted Lens Mass ($M_{\\odot}$")
        plt.savefig(f"{model}_position_mass_histogram.png")
        plt.close()

    return probability_interpolator
