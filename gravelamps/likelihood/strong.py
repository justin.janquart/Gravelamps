"""Gravelamps Strong Lensing Likelihoods

Following are functions handling likelihoods for strong lenisng anslyses 

Written by Mick Wright 2023

Classes
-------
JointGWTransient
    Implementation of the model specific joint likelihood computation for strongly lensed events

"""
import itertools

import attrs
import numpy as np

from bilby.core.likelihood import Likelihood
from bilby.gw.utils import noise_weighted_inner_product

class JointGWTransient(Likelihood):
    """
    Implementation of the model specific joint likelihood computation for strongly lensed events
    """

    @attrs.define
    class _CalculatedSNRs:
        d_inner_h = attrs.field()
        optimal_snr_squared = attrs.field()
        complex_matched_filter_snr = attrs.field()

    def __init__(self,
                 interferometers_first,
                 interferometers_second,
                 generator_first,
                 generator_second,
                 priors = None):

        super().__init__()

        self.first_image_generator = generator_first
        self.second_image_generator = generator_second

        self.first_image_ifos = interferometers_first
        self.second_image_ifos = interferometers_second

        self.priors = priors

        #To be filled by the sampler
        self.parameters = {}

        #Noise log likelihood
        self._noise_log_likelihood = self.noise_log_likelihood()

    @staticmethod
    def __image_str__(ifos, generator):
        return f"Image:\n\tInterferometers: {ifos}, Generator: {generator}"

    def __repr__(self):
        image_one = self.__image_str__(self.first_image_ifos, self.first_image_generator)
        image_two = self.__image_str__(self.second_image_ifos, self.second_image_generator)
        class_title = self.__class__.__name__

        return f"{class_title}:\n{image_one}\n{image_two}"

    @property
    def priors(self):
        """
        Prior implementation
        """
        return self._prior

    @priors.setter
    def priors(self, priors):
        if priors is not None:
            self._prior = priors.copy()
        else:
            self._prior = None

    def noise_log_likelihood(self):
        """
        Compute the noise log likeihood for the two images at the same time
        """

        log_likelihood = 0

        for interferometer in itertools.chain(self.first_image_ifos, self.second_image_ifos):
            mask = interferometer.frequency_mask
            log_likelihood -=\
                noise_weighted_inner_product(interferometer.frequency_domain_strain[mask],
                                             interferometer.frequency_domain_strain[mask],
                                             interferometer.power_spectral_density_array[mask],
                                             self.first_image_generator.duration)/2.

        return float(np.real(log_likelihood))

    def calculate_snrs(self, waveform_polarisations, interferometer, parameters):
        """
        Calculate the SNR for a detector
        """

        signal = interferometer.get_detector_response(waveform_polarisations, parameters)
        d_inner_h = interferometer.inner_product(signal=signal)
        optimal_snr_squared = interferometer.optimal_snr_squared(signal=signal)
        complex_matched_filter_snr = d_inner_h/(optimal_snr_squared**0.5)

        return self._CalculatedSNRs(d_inner_h=d_inner_h,
                                    optimal_snr_squared=optimal_snr_squared,
                                    complex_matched_filter_snr=complex_matched_filter_snr)

    def log_likelihood_image(self, polarisation, interferometers):
        """
        Calculate the contribution to the log likelihood from an individual image
        """
        d_inner_h = 0
        optimal_snr_squared = 0

        for interferometer in interferometers:
            per_detector_snr = self.calculate_snrs(waveform_polarisations=polarisation,
                                                   interferometer=interferometer,
                                                   parameters=self.parameters)

            d_inner_h += per_detector_snr.d_inner_h
            optimal_snr_squared += np.real(per_detector_snr.optimal_snr_squared)

        return np.real(d_inner_h) - optimal_snr_squared/2.

    def log_likelihood_ratio(self):
        """
        Compute the log likelihood ratio for the two lensed images
        """

        first_image_polarisations =\
            self.first_image_generator.frequency_domain_strain(self.parameters)
        second_image_polarisations =\
            self.second_image_generator.frequency_domain_strain(self.parameters)

        first_image_likelihood = self.log_likelihood_image(first_image_polarisations,
                                                           self.first_image_ifos)
        second_image_likelihood = self.log_likelihood_image(second_image_polarisations,
                                                            self.second_image_ifos)

        return float(first_image_likelihood + second_image_likelihood)

    def log_likelihood(self):
        """
        Compute the log likelihood for the event pair
        """

        return self.log_likelihood_ratio() + self._noise_log_likelihood
