// Utility function definitions for assisting in the calculation of the
// amplification factor for the isolated point mass lens model.
//
// Mick Wright 2022

#include "src/point.h"

// Wrapper converts the std::complex value of the amplification factor
// calculated by SingleImageAmplification into a pair of doubles for
// compatibility with ctypes.
//
// Input:
//      double dimensionless_frequency : dimensionless form of the frequency
//                                       being amplified
//      double source_position : dimensionless displacement from optical axis
//      int image : integer determining which image to calculate amplification
//                  factor for.
//                  1 will calculate the + image
//                  0 will calculate the - image
//
// Output:
//      double* amplification_array : array of length 2 containing the real and
//                                    imaginary parts of the amplification
//                                    factor value
double* PySingleImageAmplification(
    double dimensionless_frequency, double source_position, int image) {

    std::complex<double> amplification_factor =\
        SingleImageAmplification(dimensionless_frequency,
                                 source_position,
                                 image);

    double* amplification_array = new double[2];
    amplification_array[0] = std::real(amplification_factor);
    amplification_array[1] = std::imag(amplification_factor);

    return amplification_array;
}

// Wrapper converts the std::complex value of the amplification factor
// calculated by AmplificationFactorGeoemtric into a pair of doubles for
// compatibility with ctypes.
//
// Input:
//      double dimensionless_frequency : dimensionless form of the frequency
//                                       being amplified
//      double source_position : dimensionless displacement from optical axis
//
// Output:
//      double* amplification_array : array length 2 containing the real and
//                                    imaginary parts of the amplification
//                                    factor value
double* PyAmplificationFactorGeometric(
    double dimensionless_frequency, double source_position) {

    // Caclulate complex value using AmplificationFactorGeometric
    std::complex<double> amplification_factor =\
        AmplificationFactorGeometric(dimensionless_frequency, source_position);

    double* amplification_array = new double[2];
    amplification_array[0] = std::real(amplification_factor);
    amplification_array[1] = std::imag(amplification_factor);

    return amplification_array;
}

// Wrapper to get the magnification
//
// Input:
//      double source_position : dimensionless displacement from optical axis
//      int mode : determines whether to calculate plus or minus image based on
//                 whether 1 or not
//
// Output:
//      double magnification : magnficiation of specified image
double PyMagnification(double source_position, int mode) {
    return Magnification(source_position, mode);
}

// Wrapper to get the time delay
//
// Input:
//      double source_position : dimensionless displacement from optical axis
//
// Output:
//      double time_delay : dimensionless time delay induced by the lensing
double PyTimeDelay(double source_position) {
    return TimeDelay(source_position);
}
