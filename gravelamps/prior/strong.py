"""
Gravelamps Strong Lensing Prior Handling

Functions within are used to handle the construction of priors needed for the
`gravelamps_strong_lensing` program

Routines
--------
construct_prior_dict
    Generate a set of priors for a strong lensing event
"""

from bilby.core.result import read_in_result
from bilby.core.prior import DeltaFunction, Uniform
from bilby.gw.prior import BBHPriorDict

from gravelamps.core.gravelog import gravelogger

def construct_prior_dict(module, event_data, mass_data):
    """
    Generate a set of priors for a strong lensing event

    Parameters
    ----------
    module : module
        Module containing model lens functions
    event_data : dict
        Information on the event
    mass_data : ArrayLike
        Sample redshifted lens mass values

    Returns
    -------
    prior_dict : bilby.gw.prior.BBHPriorDict
        Dictionary styled object containing priors
    """

    gravelogger.info("Generating prior dictionary")

    prior_dict = BBHPriorDict()
    if "golum_result_file" in event_data:
        result_obj = read_in_result(filename=event_data["golum_result_file"])

        for key, value in result_obj.priors.items():
            prior_dict[key] = value
        prior_dict["luminosity_distance"].maximum *= 2
    else:
        prior_dict.from_dictionary(event_data["priors"])

    prior_dict["redshifted_lens_mass"] = Uniform(minimum=mass_data.min(),
                                                 maximum=mass_data.max(),
                                                 name="redshifted_lens_mass",
                                                 latex_label="$M_{lz}$")
    prior_dict["source_position"] = Uniform(minimum=module.SOURCE_MIN,
                                            maximum=module.SOURCE_MAX,
                                            name="source_position",
                                            latex_label="$y$")
    prior_dict["lens_fractional_distance"] = DeltaFunction(peak=0.5,
                                                           name="lens_fractional_distance")

    return prior_dict
