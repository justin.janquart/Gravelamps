"""
Gravelamps Strong Lensing Model Selection

Functions within control running Gravelamps on GOLUM produced model agnostic parameter estimation
data for potential strong lensing candidates to determine the most likely lensing model and the
corresponding most likely lensing parameters. Main funciton is accessed via 
`gravelamps_strong_lensing`
"""

import importlib
import json
import os

import numpy as np
import pandas as pd

from gravelamps.core.file_handling import (read_event_file,
                                           read_golum_reweighted_file,
                                           read_golum_result_file)
from gravelamps.core.graveparser import create_graveparser
from gravelamps.core.gravelog import setup_logger, gravelogger
try:
    from gravelamps.lensing.strong_event_handling import (get_event_signal,
                                                          get_model_generator,
                                                          generate_probability_interpolator,
                                                          observables_to_model_parameters)
except ImportError as exc:
    raise ImportError("GOLUM not available, please install it using pip") from exc
from gravelamps.likelihood.strong import JointGWTransient
from gravelamps.prior.strong import construct_prior_dict

MODEL_LIST = ["point", "sis"]
MODEL_MODULES =\
    { model : importlib.import_module(f"gravelamps.lensing.{model}") for model in MODEL_LIST }
SOURCE_PARAMETERS = ['mass_ratio', 'chirp_mass', 'luminosity_distance',
                     'dec', 'ra', 'theta_jn', 'psi', 'phase', 'a_1', 'a_2',
                     'tilt_1', 'tilt_2', 'phi_12', 'phi_jl', 'geocent_time', 'n_phase']
LENS_OBSERVABLES = ['relative_magnification', 'delta_t', 'delta_n']

def main():
    """
    Read in event information and using this information about model agnostic parameter estimation
    samples, calculate evidence for specific lensing models.

    In generating the evidence for the specific lensing model, estimation of the mode specific
    parameters will also be performed.
    """

    graveparser = create_graveparser()
    args = graveparser.parse_args()

    setup_logger(outdir=".", log_level="INFO")

    event_data = read_event_file(args)
    event_information = {}

    for event, event_dict in event_data.items():
        gravelogger.info("Running on event: %s", event)
        event_information[f"{event}"] = {}

        if "golum_reweight_file" in event_dict:
            golum_reweighted_file = event_dict["golum_reweight_file"]

            if not os.path.isfile(event_dict["golum_reweight_file"]):
                gravelogger.warning("%s not found, skipping", event_dict["golum_reweight_file"])
                continue

            golum_source_parameters, golum_lens_observables =\
                read_golum_reweighted_file(golum_reweighted_file)

        elif "golum_result_file" in event_dict:
            golum_result_file = event_dict["golum_result_file"]

            if not os.path.isfile(event_dict["golum_result_file"]):
                gravelogger.warning("%s not found, skipping", event_dict["golum_result_file"])
                continue

            golum_source_parameters, golum_lens_observables =\
                read_golum_result_file(golum_result_file, SOURCE_PARAMETERS, LENS_OBSERVABLES)

        else:
            raise ValueError("No supported result file specified!")

        gravelogger.info("Generating event signal")
        interferometers_first, interferometers_second = get_event_signal(args, event_dict)
        model_evidence_dict = {}

        for model in MODEL_LIST:
            gravelogger.info("Calculating evidence for model: %s", model)

            lens_module = MODEL_MODULES[model]
            model_waveform_generator_first, model_waveform_generator_second =\
                get_model_generator(args, lens_module, event_dict)

            source_position_data, lens_mass_data =\
                observables_to_model_parameters(lens_module,
                                                args,
                                                golum_lens_observables["relative_magnification"],
                                                golum_lens_observables["delta_t"])

            probability_interpolator =\
                generate_probability_interpolator(source_position_data,
                                                  lens_mass_data,
                                                  number_of_bins = args.n_bins,
                                                  plot=args.plot,
                                                  model=model)

            prior_dict = construct_prior_dict(lens_module, event_dict, lens_mass_data)
            likelihood = JointGWTransient(interferometers_first,
                                          interferometers_second,
                                          model_waveform_generator_first,
                                          model_waveform_generator_second)

            sample_evidences = []

            for idx, (position, mass) in enumerate(zip(source_position_data, lens_mass_data)):
                if not (idx + 1) % args.sample_dist == 0:
                    continue

                print(f"Sample {idx+1} of {len(source_position_data)}", end="\r")

                parameter_log_probability = np.log(probability_interpolator(position, mass))
                lens_parameter_dict = {"lens_fractional_distance": 0.5,
                                           "source_position": position,
                                           "redshifted_lens_mass": mass}
                source_parameter_dict =\
                    {key : value[idx] for key, value in golum_source_parameters.items()\
                                      if key in SOURCE_PARAMETERS}
                source_parameter_dict.pop("n_phase")
                parameter_dict = source_parameter_dict | lens_parameter_dict

                magnification = lens_module.magnification(position, 1)

                parameter_dict["luminosity_distance"] *= np.sqrt(abs(magnification))

                likelihood.parameters = parameter_dict
                sample_likelihood = likelihood.log_likelihood()

                sample_prior = prior_dict.ln_prob(parameter_dict)

                sample_evidence_contribution = sample_likelihood + sample_prior\
                                               - parameter_log_probability

                if not np.isinf(sample_evidence_contribution) and\
                    not np.isnan(sample_evidence_contribution):
                    sample_evidences.append(float(sample_evidence_contribution))
                else:
                    print(f"Parameter Prob: {parameter_log_probability}")
                    print(f"Lens Parameters: {lens_parameter_dict}")

            model_evidence = np.mean(sample_evidences)
            gravelogger.info("Evidence for %s : %f", model, model_evidence)
            model_evidence_dict[f"{model}"] = model_evidence
            event_information[f"{event}"][f"{model}_samples"] = sample_evidences
            event_information[f"{event}"][f"{model}_mass_data"] = lens_mass_data
            event_information[f"{event}"][f"{model}_source_position_data"] = source_position_data

        preferred_model = max(model_evidence_dict, key=model_evidence_dict.get)
        gravelogger.info("Event %s is most likely: %s", event, preferred_model)

        event_information[f"{event}"]["model_evidences"] = model_evidence_dict
        event_information[f"{event}"]["preferred_model"] = preferred_model

    gravelogger.info("Saving information to strong_lensing_model_selection.json")
    frame = pd.DataFrame(event_information)

    with open("strong_lensing_model_selection.json", "w", encoding="utf-8") as dump_file:
        json.dump(json.loads(frame.to_json()), dump_file, indent=4)

if __name__ == "__main__":
    main()
